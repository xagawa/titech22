# See https://pycryptodome.readthedocs.io/en/latest/index.html
# pip install pycryptodome 
# or pip3 install pycryptodome
from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Util.number import bytes_to_long, long_to_bytes, inverse
from hashlib import sha1, sha256
import math

def CRTinv(xp,xq,u,p,q,n):
	return (xp + (((xq-xp) * u) % q) * p) % n

def myhash(msg):
	hsh1 = sha256(("1"+msg).encode()).digest()
	hsh2 = sha256(("2"+msg).encode()).digest()
	hsh3 = sha256(("3"+msg).encode()).digest()
	hsh4 = sha256(("4"+msg).encode()).digest()
	return bytes_to_long(hsh1+hsh2+hsh3+hsh4)

if __name__=="__main__":

	with open("3a-rsa-pub.pem",'rb') as key:
		pubkey = RSA.importKey(key.read())
	n = pubkey.n
	e = pubkey.e
	print("3a-rsa-pub-pub.pem")
	print(f"n: {n:#x}")
	print(f"e: {e:#x}")

	with open("3a-rsa-priv.key",'rb') as key:
		privkey = RSA.importKey(key.read())
	n = privkey.n
	e = privkey.e
	d = privkey.d
	p = privkey.p
	q = privkey.q
	u = privkey.u # p^{-1} mod q
	print(f"n: {n:#x}")
	print(f"e: {e:#x}")
	print(f"d: {d:#x}")
	print(f"p: {p:#x}")
	print(f"q: {q:#x}")
	print(f"u: {u:#x}")
	
	msg = "https://gitlab.com/xagawa/titech22"
	h = myhash(msg)
	print(f"M: {h:#x}")

	sigma1 = faulty_pow(h,d,p) ## Oops! 
	sigma2 = pow(h,d,n)
	
	print(f"s1: {sigma1:#x}")
	print(f"s2: {sigma2:#x}")
	
	with open("3a-hash-value",'w') as HHH:
		HHH.write(f'{h:#x}')

	with open("3a-rsa-sig.cert",'w') as cert:
		cert.write(f'{sigma1:#x}')
		
	# with open("3a-rsa-sig2.cert",'w') as cert2:
	# 	cert2.write(f'{sigma2:#x}')
